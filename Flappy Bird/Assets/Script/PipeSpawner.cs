﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    [SerializeField] private Birds birds;
    [SerializeField] private Pipe pipUp, pipDown;
    [SerializeField] private float spawnInterval = 1;

    [SerializeField] public float holeSizeMin = 1f;
    [SerializeField] public float holeSizeMax = 2f;
    [SerializeField] private float maxMinOffset = 1;

    [SerializeField] private Point point;

    private Coroutine CR_Spawn;

    // Start is called before the first frame update
    void Start()
    {
        StartSpawn();
    }

    void StartSpawn()
    {
        if (CR_Spawn == null)
        {
            CR_Spawn = StartCoroutine(IeSpawn());
        }
    }

    void StopSpawn()
    {
        if (CR_Spawn != null)
        {
            StopCoroutine(CR_Spawn);
        }
    }

    void SpawnPipe()
    {
        float y = maxMinOffset * Mathf.Sin(Time.time);

        Pipe newPipeUp = Instantiate(pipUp, transform.position, Quaternion.Euler(0, 0, 180));
        newPipeUp.transform.position += Vector3.up * y;
        //newPipeUp.transform.position += Vector3.up * (holeSize / 2);
        newPipeUp.transform.position += Vector3.up * (Random.Range(holeSizeMin,holeSizeMax) / 2);
        newPipeUp.gameObject.SetActive(true);

        Pipe newPipeDown = Instantiate(pipDown, transform.position, Quaternion.identity);
        newPipeDown.transform.position += Vector3.up * y;
        //newPipeDown.transform.position += Vector3.down * (holeSize / 2);
        newPipeDown.transform.position += Vector3.down * (Random.Range(holeSizeMin, holeSizeMax) / 2);
        newPipeDown.gameObject.SetActive(true);

        Point newPoint = Instantiate(point, transform.position, Quaternion.identity);
        newPoint.gameObject.SetActive(true);
        //newPoint.SetSize(Random.Range(holeSizeMin, holeSizeMax));
        newPoint.SetSize(9);
        newPoint.transform.position += Vector3.up * y;
    }

    IEnumerator IeSpawn()
    {
        while (true)
        {
            if (birds.IsDead())
            {
                StopSpawn();
            }

            SpawnPipe();

            yield return new WaitForSeconds(spawnInterval);
        }
    }
}
