﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Birds : MonoBehaviour
{
    [SerializeField] private int score;
    //[SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Text scoreText;

    [SerializeField] private float upForve;
    [SerializeField] private bool isDead;
    [SerializeField] private Bullet bullet;
    [SerializeField] private UnityEvent onJump, onDead;

    [SerializeField] private UnityEvent onAddPoint;

    private Rigidbody2D rigidbody2D;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();

        animator = GetComponent<Animator>();

        StartCoroutine(IeShoot());
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead && Input.GetMouseButtonDown(0))
        {
            Jump();
        }
    }

    public bool IsDead()
    {
        return isDead;
    }

    public void Dead()
    {
        if (!isDead && onDead != null )
        {
            onDead.Invoke();
        }

        isDead = true;
    }

    void Jump()
    {
        if (rigidbody2D)
        {
            rigidbody2D.velocity = Vector2.zero;
            rigidbody2D.AddForce(new Vector2(0, upForve));
        }

        if (onJump != null)
        {
            onJump.Invoke();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        animator.enabled = false;
    }

    public void AddScore(int value)
    {
        score += value;
        if (onAddPoint != null)
        {
            onAddPoint.Invoke();
        }
        scoreText.text = score.ToString();
        //scoreText.GetComponent<Text>().color = Color.black;
        //GetComponent<Text>().color = Color.gray;
    }

    private void Shoot()
    {
        Instantiate(bullet, transform.position , Quaternion.identity);
        bullet.gameObject.SetActive(true);
    }

    IEnumerator IeShoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);

            if (isDead == true)
            {
                StopCoroutine(IeShoot());
            }
            else
            {
                Shoot();
            }

        }
    }
}
