﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]

public class Ground : MonoBehaviour
{
    [SerializeField] private Birds birds;
    [SerializeField] private float speed = 1;
    [SerializeField] private Transform nextPos;

    // Update is called once per frame
    private void Update()
    {
        if (birds == null || (birds != null && !birds.IsDead()))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }

    public void SetNextGround(GameObject ground)
    {
        if (ground != null)
        {
            ground.transform.position = nextPos.position;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (birds != null && !birds.IsDead())
        {
            birds.Dead();
        }
    }
}
