﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pipe : MonoBehaviour
{
    [SerializeField] private Birds birds;
    [SerializeField] private float speed = 1 ;

    private void Update()
    {
        if (!birds.IsDead())
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Birds birds = collision.gameObject.GetComponent<Birds>();

        if (birds)
        {
            Collider2D collider = GetComponent<Collider2D>();

            if (collider)
            {
                collider.enabled = false;
            }
        }
        birds.Dead();
    }

    
}
