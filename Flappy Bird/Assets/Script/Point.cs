﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]

public class Point : MonoBehaviour
{
    [SerializeField] private Birds birds;
    [SerializeField] private float speed = 1;

    void Update()
    {
        if (!birds.IsDead())
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }

    public void SetSize(float size)
    {
        BoxCollider2D collider = GetComponent<BoxCollider2D>();

        if (collider != null)
        {
            collider.size = new Vector2(collider.size.x, size);
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        Birds birds = collision.gameObject.GetComponent<Birds>();

        if (birds && !birds.IsDead())
        {
            birds.AddScore(1);
        }
    }
}
